import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { User } from './user/user.entity';
import { UserModule } from './user/user.module';
import { AssigmentModule } from './assigment/assigment.module';
import { Assigment } from './assigment/assigment.entity';
import { TodolistModule } from './todolist/todolist.module';
import { Todolist } from './todolist/entities/todolist.entity';

@Module({
  imports: [TypeOrmModule.forRoot({
    type: 'mysql',
    host: 'localhost',
    port: 3306,
    username: 'root',
    password: 'password',
    database: 'library',
    entities: [User, Assigment, Todolist],
    synchronize: true,
    dropSchema: true
  }), UserModule, AssigmentModule, TodolistModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
