import { Body, Controller, Get, HttpStatus, Param, Post, Res } from '@nestjs/common';
import { Assigment } from './assigment.entity';
import { AssigmentService } from './assigment.service';

@Controller('assigment')
export class AssigmentController {
    constructor(private readonly assigmentService: AssigmentService) { }

    @Post()
    async createAssigment(@Res() response, @Body() assigment: Assigment) {
        const newAssigment = await this.assigmentService.createAssigment(assigment);
        return response.status(HttpStatus.CREATED).json({
            newAssigment
        })
    }

    @Get()
    async fetchAll(@Res() response) {
        const assigments = await this.assigmentService.findAll();
        return response.status(HttpStatus.OK).json({
            assigments
        })
    }

    @Get('/:id')
    async findById(@Res() response, @Param('id') id) {
        const Assigment = await this.assigmentService.findOne(id);
        return response.status(HttpStatus.OK).json({
            Assigment
        })
    }
}
