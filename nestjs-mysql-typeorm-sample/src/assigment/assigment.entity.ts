import { Column, Entity, JoinColumn, ManyToMany, PrimaryGeneratedColumn } from "typeorm";
import { Todolist } from "../todolist/entities/todolist.entity";

@Entity()
export class Assigment {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    text: string;

    @Column()
    finished: boolean;

    @ManyToMany(() => Todolist, (todolist) => todolist.assigments)
    @JoinColumn({ name: 'todo_id', referencedColumnName: 'id' })
    todoList: Todolist;
}