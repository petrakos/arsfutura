import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Assigment } from './assigment.entity';
import { AssigmentService } from './assigment.service';
import { AssigmentController } from './assigment.controller';

@Module({
    imports: [TypeOrmModule.forFeature([Assigment])],
    providers: [AssigmentService],
    controllers: [AssigmentController]
})
export class AssigmentModule { }
