import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Assigment as Assignment } from './assigment.entity';

@Injectable()
export class AssigmentService {
    constructor(
        @InjectRepository(Assignment)
        private assignmentRepository: Repository<Assignment>
    ) { }

    findAll(): Promise<Assignment[]> {
        return this.assignmentRepository.find();
    }

    async findOne(id: number): Promise<Assignment> {
        const assignments: Assignment[] = await this.assignmentRepository.find();
        //console.log("Users: ");
        //console.log(users)
        for (let i = 0; i < assignments.length; i++) {
            const element = assignments[i];
            if (element.id == id)
                return element;
        }

        throw new Error("There is no user with id " + id);
    }

    createAssigment(assailment: Assignment): Promise<Assignment> {
        return this.assignmentRepository.save(assailment);
    }
}
