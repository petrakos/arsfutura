import { Column, Entity, JoinColumn, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Assigment } from "../../assigment/assigment.entity";

@Entity()
export class Todolist {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @OneToMany(() => Assigment, (assigment) => assigment.todoList, {
        cascade: true
    })
    @JoinColumn()
    assigments: Assigment[];


}
