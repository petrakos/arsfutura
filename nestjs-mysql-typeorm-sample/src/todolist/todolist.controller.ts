import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus } from '@nestjs/common';
import { TodolistService } from './todolist.service';
import { Todolist } from './entities/todolist.entity';

@Controller('todolist')
export class TodolistController {
  constructor(private readonly todolistService: TodolistService) { }

  @Post()
  create(@Res() response, @Body() createTodolist: Todolist) {
    const todo = this.todolistService.create(createTodolist);
    return response.status(HttpStatus.OK).json({ todo });
  }

  @Get()
  findAll(@Res() response) {
    const todo = this.todolistService.findAll();
    return response.status(HttpStatus.OK).json({ todo });
  }

  @Get(':id')
  findOne(@Res() response, @Param('id') id: number) {
    const todo = this.todolistService.findOne(id);
    return response.status(HttpStatus.OK).json({ todo })
  }


  @Delete(':id')
  remove(@Res() response, @Param('id') id: number) {
    this.todolistService.remove(id);
    return response.status(HttpStatus.OK)
  }
}
