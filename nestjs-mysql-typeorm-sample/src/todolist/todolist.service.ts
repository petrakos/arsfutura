import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Console } from 'console';
import { Repository } from 'typeorm';
import { Assigment } from '../assigment/assigment.entity';
import { CreateTodolistDto } from './dto/create-todolist.dto';
import { UpdateTodolistDto } from './dto/update-todolist.dto';
import { Todolist } from './entities/todolist.entity';

@Injectable()
export class TodolistService {

  constructor(
    @InjectRepository(Todolist) private todolistRepository: Repository<Todolist>,
  ) { }

  create(todolist: Todolist): Promise<Todolist> {
    console.log(todolist);
    return this.todolistRepository.save(todolist);
  }

  findAll(): Promise<Todolist[]> {
    return this.todolistRepository.find();
  }

  async findOne(id: number): Promise<Todolist> {
    const todolists: Todolist[] = await this.todolistRepository.find();
    //console.log("Users: ");
    //console.log(users)
    for (let i = 0; i < todolists.length; i++) {
      const element = todolists[i];
      if (element.id == id)
        return element;
    }

    throw new Error('There is no user with id: ' + id);
  }


  remove(id: number) {
    this.todolistRepository.delete(id);
  }
}
