import { BadRequestException, Body, Controller, Get, HttpStatus, Param, ParseIntPipe, Post, Res } from '@nestjs/common';
import { User } from './user.entity';
import { LoginUserDTO } from './user.login-user.dto';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
    constructor(private readonly usersService: UserService) { }

    @Post()
    async createUser(@Res() response, @Body() user: User): Promise<User> {
        const data = await this.usersService.createUser(user);
        return response.status(HttpStatus.CREATED).json({
            data
        })
    }

    @Post('/login')
    async loginUser(@Res() response, @Body() user: LoginUserDTO) {
        try {
            const email = await this.usersService.loginUser(user);
            console.log(email);
            return response.status(HttpStatus.OK).json({
                email
            })
        } catch (_error) {
            let error = _error.message;
            return response.status(HttpStatus.BAD_REQUEST).json({
                error
            })
        }

    }

    @Get()
    async fetchAll(@Res() response): Promise<User> {
        const users = await this.usersService.findAll();
        return response.status(HttpStatus.OK).json({
            users
        })
    }

    @Get('/:id')
    async findById(@Res() response, @Param('id', ParseIntPipe) id: number) {
        try {
            const user = await this.usersService.findOne(id);
            return response.status(HttpStatus.OK).json({
                user
            })
        } catch (_error) {
            let error = _error.message;
            return response.status(HttpStatus.BAD_REQUEST).json({
                error
            })

        }

    }
}
