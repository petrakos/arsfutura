import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';
import { LoginUserDTO } from './user.login-user.dto';

@Injectable()
export class UserService {




    constructor(
        @InjectRepository(User)
        private userRepository: Repository<User>
    ) { }

    public async loginUser(user: LoginUserDTO): Promise<string | undefined> {
        const users: User[] = await this.userRepository.find();
        for (let i = 0; i < users.length; i++) {
            const element = users[i];
            if (element.email === user.email) {
                if (element.password === user.password) {
                    return element.email;
                }
                throw new Error("Wrong password!");
            }

        }
        throw new Error("There is no user with email ");

    }


    public async findOne(id: number): Promise<User> {
        //TODO: pokusaj skuziti zasto ovo ne radi
        //const user = this.userRepository.findOne(id);
        const users: User[] = await this.userRepository.find();
        //console.log("Users: ");
        //console.log(users)
        for (let i = 0; i < users.length; i++) {
            const element = users[i];
            if (element.id == id)
                return element;
        }

        throw new Error("There is no user with id " + id);

    }

    findAll(): Promise<User[]> {
        return this.userRepository.find();
    }

    // findByEmail(email: string): Promise<User | undefined> {
    //     return this.userRepository.findOne({email});
    // }

    createUser(user: User): Promise<User> {
        return this.userRepository.save(user);
    }
}
