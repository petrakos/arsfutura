import { useNavigate } from "react-router-dom";
import axios from "axios";
import React, { Component, useEffect, useState } from "react";
import background from "../resources/pexels-tirachard-kumtanom-733852.jpg";

const Auth = (props) => {
  let [authMode, setAuthMode] = useState("signin");
  const navigate = useNavigate();
  const [fullNameInputRef, setFullNameInputRef] = useState("");
  const [emailInputRef, setEmailInputRef] = useState("");
  const [passwordInputRef, setPasswordInputRef] = useState("");
  const emailFromLocal = localStorage.getItem("email");
  console.log(emailFromLocal);

  useEffect(() => {
    if (emailFromLocal != null) {
      navigate("/homepage");
    }
  });

  const changeAuthMode = () => {
    setAuthMode(authMode === "signin" ? "signup" : "signin");
  };

  const handleInputFullName = (event) => {
    setFullNameInputRef(event.target.value);
    console.log("Full name: " + fullNameInputRef);
  };

  const handleInputEmail = (event) => {
    setEmailInputRef(event.target.value);
    console.log("Email: " + emailInputRef);
  };

  const handleInputPass = (event) => {
    setPasswordInputRef(event.target.value);
    console.log("Password: " + passwordInputRef);
  };

  const handleSubmit = () => {
    console.log(authMode);
    if (authMode === "signup") {
      const data = {
        fullName: fullNameInputRef,
        email: emailInputRef,
        password: passwordInputRef,
      };

      axios
        .post("http://localhost:3000/user", data)
        .then((response) => {
          console.log(response.status);
          console.log(response.data);
          const email = response.data.email;
          console.log(email);
          localStorage.setItem("email", email);
        })
        .catch((error) => {
          console.error(error.response.data);
          alert(error.response.data.error);
        });
      navigate("/homepage");
    }

    const data = {
      email: emailInputRef,
      password: passwordInputRef,
    };

    axios
      .post("http://localhost:3000/user/login", data)
      .then((response) => {
        console.log(response.status);
        // if (response.status == ) {

        // }
        console.log(response.data);
        const email = response.data.email;
        console.log(email);
        localStorage.setItem("email", email);
      })
      .catch((error) => {
        console.error(error.response.data);
        alert(error.response.data.error);
      });
    navigate("/homepage");
  };

  return (
    <div
      style={{
        backgroundImage: `url(${background})`,
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
      }}
      className="Auth-form-container"
    >
      <form className="Auth-form" style={{}}>
        <div className="Auth-form-content">
          <h3 className="Auth-form-title">
            {authMode === "signup" ? <p>Sign Up</p> : <p>Sign In</p>}{" "}
          </h3>
          <div className="text-center">
            Already registered?{" "}
            <span className="link-primary" onClick={changeAuthMode}>
              {!(authMode === "signup") ? (
                <button type="button" class="btn btn-link">
                  Sign Up
                </button>
              ) : (
                <button type="button" class="btn btn-link">
                  Sign In
                </button>
              )}
            </span>
          </div>
          {authMode === "signup" && (
            <div className="form-group mt-3">
              <label>Full Name</label>
              <input
                name="fullNameInputRef"
                onChange={handleInputFullName}
                className="form-control mt-1"
                placeholder="e.g Jane Doe"
              />
            </div>
          )}
          <div className="form-group mt-3">
            <label>Email address</label>
            <input
              type="email"
              name="emailInputRef"
              onChange={handleInputEmail}
              className="form-control mt-1"
              placeholder="Email Address"
            />
          </div>
          <div className="form-group mt-3">
            <label>Password</label>
            <input
              type="password"
              name="passwordInputRef"
              onChange={handleInputPass}
              className="form-control mt-1"
              placeholder="Password"
            />
          </div>
          <div className="d-grid gap-2 mt-3">
            <button
              //type="submit"
              className="btn btn-primary"
              onClick={handleSubmit}
            >
              Submit
            </button>
          </div>
          <p className="text-center mt-2">
            Forgot <a href="#">password?</a>
          </p>
        </div>
      </form>
    </div>
  );
};

export default Auth;
