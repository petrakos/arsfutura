import axios from "axios";
import React, { useEffect, useState } from "react";
import { Card, Stack, Button, ListGroup } from "react-bootstrap";
import { MdDeleteOutline } from "react-icons/md";
import { BiAddToQueue } from "react-icons/bi";

const TodoList = (props) => {
  const [value, setValue] = useState("");
  const [todos, setTodos] = useState(props.lists);
  const [input, setInput] = useState("");

  useEffect(() => {
    console.log("todos");
    console.log(todos);
  }, []);

  const check = (index, finished) => {
    const newTodos = [...todos];
    newTodos[index].finished = !finished;
    console.log("novi todo");
    console.log(newTodos);
    //set(newTodos);
    setTodos(newTodos);
    console.log("new assigment in todo form:");
    console.log(newTodos[index]);
    axios.post("http://localhost:3000/assigment/update", newTodos[index]);
  };

  const remove = (index) => {
    const newTodos = [...todos];

    console.log("Remove assigment:");
    console.log(newTodos[index]);
    console.log(newTodos[index].id);

    axios.delete("http://localhost:3000/assigment/" + newTodos[index].id);
    newTodos.splice(index, 1);
    setTodos(newTodos);
  };

  const addTodo = (text) => {
    const newTodos = [...todos, { text }];
    setTodos(newTodos);
  };

  const handleInput = (event) => {
    setInput(event.target.value);
    console.log("New input: " + input);
  };

  const handleSubmit = () => {
    //this _id is from todo list
    const data = {
      id: props.todoId,
      text: input,
      finished: false,
    };
    console.log("add new assigment:");
    console.log(data);
    //axios.post("http://localhost:3000/assigment", data).then((response) => {
    //this data would be like data in line 53 but with id from database that was generated for this assigment
    //  data = response.data;
    //});
    let newTodos = [];
    if (todos) {
      newTodos = [...todos, data];
    } else {
      newTodos = [data];
    }
    setTodos(newTodos);
    console.log("svi todo:");
    console.log(todos);
  };

  return (
    <Stack
      className="Auth-form"
      style={{ justifyContent: "center", margin: "25px" }}
    >
      <ListGroup>
        <h1 className="text-center mb-4"> {props.title}</h1>

        {todos &&
          todos.map((todo, index) => (
            <ListGroup.Item variant="outline-dark">
              <Stack
                style={{
                  textDecoration: todo.finished ? "line-through" : "",
                  width: "500px",
                }}
                direction="horizontal"
                gap={4}
              >
                <input
                  class="form-check-input me-1"
                  type="checkbox"
                  value="true"
                  style={{
                    accentColor: "black",
                    color: "black",
                    backgroundColor: "black",
                  }}
                  aria-label="..."
                  onClick={() => check(index, todo.finished)}
                  checked={todo.finished}
                />
                {todo.text}
                <Button
                  variant="dark"
                  onClick={() => remove(index)}
                  style={{ alignSelf: "self-end", alignContent: "end" }}
                >
                  {" "}
                  <MdDeleteOutline></MdDeleteOutline>
                </Button>
              </Stack>
            </ListGroup.Item>
          ))}
        <Stack direction="horizontal">
          <input type="text" onChange={handleInput} />
          <Button
            variant="dark"
            onClick={() => handleSubmit()}
            style={{ marginLeft: "5px" }}
          >
            {" "}
            <BiAddToQueue></BiAddToQueue> Add new item{" "}
          </Button>
        </Stack>
      </ListGroup>
    </Stack>
  );
};

export default TodoList;
