import axios from "axios";
import React, { useEffect, useState } from "react";
import TodoList from "../components/TodoList";
import background from "../resources/pexels-tirachard-kumtanom-733852.jpg";
import { Container, Row, Col, Button } from "react-bootstrap";

const HomePage = (props) => {
  const [lists, setlists] = useState([]);
  const [title, setTitle] = useState([]);
  const [input, setInput] = useState("");
  const email = localStorage.getItem("email");

  const [inputData, setInputData] = useState({
    todo: [
      {
        title: "vecera",
        id: 1,
        assigments: [
          {
            id: 1,
            text: "prvi zadatak",
            finished: false,
          },
          {
            id: 2,
            text: "prvi zadatak",
            finished: false,
          },
          {
            id: 3,
            text: "treci zadatak",
            finished: true,
          },
          {
            id: 4,
            text: "prvi zadatak",
            finished: false,
          },
        ],
      },
      {
        id: 2,
        title: "rucak",
        assigments: [
          {
            id: 5,
            text: "prvdsfghi zadatak",
            finished: false,
          },
          {
            id: 6,
            text: "prvi zaddsfghfdatak",
            finished: false,
          },
          {
            id: 7,
            text: "treci zadatagfdk",
            finished: true,
          },
          {
            id: 8,
            text: "prvi zadatwerfdswak",
            finished: false,
          },
        ],
      },
      {
        id: 3,
        title: "vanja",
        assigments: [
          {
            id: 9,
            text: "prvdsfghi zadatak",
            finished: false,
          },
          {
            id: 10,
            text: "prvi zaddsfghfdatak",
            finished: false,
          },
        ],
      },
      {
        id: 4,
        title: "jasna",
        assigments: [
          {
            id: 11,
            text: "prvdsfghi zadatak",
            finished: false,
          },
          {
            id: 12,
            text: "prvi zaddsfghfdatak",
            finished: false,
          },
          {
            id: 13,
            text: "treci zadatagfdk",
            finished: true,
          },
          {
            id: 14,
            text: "prvi zadatwerfdswak",
            finished: false,
          },
        ],
      },
    ],
  });

  const handleNewList = (event) => {
    setInput(event.target.value);
    console.log("New input: " + input);
  };

  const addNewList = () => {
    const data = {
      email: email,
      id: 23,
      title: input,
      assigment: [],
    };
    console.log("add new todolist:");
    console.log(data);
    //axios.post("http://localhost:3000/todo", data).then((response) => {
    //this data would have id from todo and title and empty array off assigments
    //  data = response.data;
    //});
    const newTodos = [...inputData.todo, data];
    inputData.todo = newTodos;
    console.log("svi todo:");
    console.log(inputData);
  };

  useEffect(() => {
    axios
      .get("http://localhost:3000/todolist")
      .then((response) => {
        //console.log(response.status);
        //console.log(response.data);
        //console.log(email);
        setlists(response.data.todo.assigments);
        setlists(response.data.assigments);
      })
      .catch((error) => {
        console.error(error.response.data);
        alert(error.response.data.error);
      });
  }, []);

  return (
    <div
      style={{
        backgroundImage: `url(${background})`,
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
      }}
      className="Auth-form-container"
    >
      <Container fluid>
        {inputData.todo.map((todo, index) => (
          <TodoList
            key={index}
            title={todo.title}
            todoId={todo.id}
            lists={todo.assigments}
          />
        ))}{" "}
        <Row className="justify-content-md-center">
          <Col xs>
            <h3>Add new todo list:</h3>
          </Col>
          <Col xs style={{ alignSelf: "self-start" }}>
            <input type="text" onChange={handleNewList} />
          </Col>
          <Col xs>
            <Button onClick={() => addNewList()} variant="dark">
              Submit!
            </Button>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default HomePage;
